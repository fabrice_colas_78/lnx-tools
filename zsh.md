# zsh

## Installation

```bash
sudo apt-get update && sudo apt-get install -y zsh
```

## Add Oh-My-ZSH

curl

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

wget

```bash
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

## Change your default shell

```bash
chsh -s $(which zsh)
```

## Auto suggestions

```bash
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

Add the plugin to the list of plugins (awa kubectl) for Oh My Zsh to load (inside ~/.zshrc):

```bash
sed "s/plugins=(git/plugins=(git zsh-autosuggestions kubectl/g" -i.bak ~/.zshrc
```

## Change prompt

```bash
cp -p ~/.oh-my-zsh/themes/robbyrussell.zsh-theme ~/.oh-my-zsh/themes/robbyrussell.zsh-theme.`date "+%Y%m%d-%H%M%S"`.save
cat << \EOF >~/.oh-my-zsh/themes/robbyrussell.zsh-theme
PROMPT='%{$fg_bold[magenta]%}%n@%m %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)'
PROMPT+="%(?:%{$fg_bold[green]%}➤➤ :%{$fg_bold[red]%}➤➤ )"
PROMPT+="%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
EOF
```

Reload zsh

```bash
source ~/.zshrc
```

## Powerline

```bash
sudo apt update && sudo apt install -y powerline
```

## All in one

```bash
echo Installing zsh...
sudo apt-get update && sudo apt-get install -y zsh

echo Installing Oh-My-Zsh...
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo Registering Zsh as your default shell...
chsh -s $(which zsh)

echo Installing "auto-suggestions" plugin...
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

echo Registering "auto-suggestions/kubectl" plugin...
sed "s/plugins=(git/plugins=(git zsh-autosuggestions kubectl/g" -i.bak .zshrc

echo Changing prompt...
cp -p ~/.oh-my-zsh/themes/robbyrussell.zsh-theme ~/.oh-my-zsh/themes/robbyrussell.zsh-theme.`date "+%Y%m%d-%H%M%S"`.save
cat << \EOF >~/.oh-my-zsh/themes/robbyrussell.zsh-theme
PROMPT='%{$fg_bold[magenta]%}%n@%m %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)'
PROMPT+="%(?:%{$fg_bold[green]%}➤➤ :%{$fg_bold[red]%}➤➤ )"
PROMPT+="%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
EOF

echo Reloading zsh...
source ~/.zshrc
```

# References

* Oh-My-Zsh  https://github.com/ohmyzsh/ohmyzsh
* Auto suggestions https://github.com/zsh-users/zsh-autosuggestions
* lxc completion with zsh https://gronono.fr/2019/05/ohmyzsh_lxc
* Unicode characters table https://unicode-table.com/en/  / https://unicode-table.com/en/#geometric-shapes-extended
