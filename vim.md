# VIM

## Install ```vim-airline``` (w/ pathogen)

Install ___pathogen___ (if needed)

```bash
mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
```

Install ___vim-airline___ and ___vim-airline themes___

```bash
git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline
git clone https://github.com/vim-airline/vim-airline-themes ~/.vim/bundle/vim-airline-themes
```

Install ___vim-fugitive___ in order to see your git branch you are working on

```bash
git clone https://github.com/tpope/vim-fugitive.git ~/.vim/bundle/vim-fugitive
```

### ```vim-airline``` themes list

* https://github.com/vim-airline/vim-airline/wiki/Screenshots