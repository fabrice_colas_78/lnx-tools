#!/bin/bash

GIT_REPO=https://gitlab.com/fabrice_colas_78/lnx-tools.git

COMMON_PACKAGES="git tmux vim curl"

is_root=$(id -u)

if [ _${NO_INSTALL_COMMON_PACKAGES}_ != _1_ ]; then
  sudo -v >/dev/null 2>&1
  has_sudo=$?
  if [ $has_sudo -eq 0 ]; then
    echo "Installing $COMMON_PACKAGES as current user $USER has sudo priviliges..."
    if [[ $(cat /etc/*release | grep "^ID=.*$") =~ "centos" ]] || [[ $(cat /etc/*release | grep "^ID=.*$") =~ "rhel" ]]; then 
      sudo yum update -y && sudo yum install -y ${COMMON_PACKAGES}
    elif [[ $(cat /etc/*release | grep "^ID=.*$") =~ "ubuntu" ]]; then 
      sudo apt update -y  && sudo apt install -y ${COMMON_PACKAGES}
    elif [[ $(cat /etc/*release | grep "^ID=.*$") =~ "bian" ]]; then 
      sudo apt update -y  && sudo apt install -y ${COMMON_PACKAGES}
    else
      echo "OS not supported"
      exit 1
    fi
  fi
fi

which git >/dev/null
has_git=$?
which tmux >/dev/null
has_tmux=$?

if [ $has_git -ne 0 ]; then
  echo "Error: git tool is not installed in this box, exiting..."
  exit 1
fi

if [ $has_tmux -ne 0 ]; then
  echo "Warning: 'tmux' tool is not installed in this box."
fi

mkdir -p ~/.local && cd ~/.local
if [ ! -d lnx-tools ]; then
  git clone $GIT_REPO
  if [ $? -ne 0 ]; then
    echo "Error: Unable to clone ${GIT_REPO}"
    exit 1
  fi
  cd lnx-tools
else
  cd lnx-tools
  git pull --no-rebase
fi
source dotfiles/myextrafuncs
git_set_local_user

cd ~

# Update ~/.ssh/rc file for setting SSH auth socket location for TMUX
grep 'Fix SSH auth socket' ~/.ssh/rc >/dev/null 2>&1
RES_GREP=`echo $?`
if [ $RES_GREP -ne 0 ]; then
  SSH_RC=~/.ssh/rc
  mkdir -p ~/.ssh
  echo Updating $SSH_RC for setting SSH auth socket location ...
  echo >>$SSH_RC
  echo "# Fix SSH auth socket location so agent forwarding works with tmux" >>$SSH_RC
  echo "if test \"\$SSH_AUTH_SOCK\" ; then" >>$SSH_RC
  echo "  ln -sf \$SSH_AUTH_SOCK ~/.ssh/ssh_auth_sock" >>$SSH_RC
  echo "fi" >>$SSH_RC
fi

# update .bashrc
grep "dotfiles/myrc" ~/.bashrc >/dev/null 2>&1
if [ $? -eq 1 ]; then
  echo "Adding lnx-tools resources to .bashrc..."
  
  if [ $is_root -eq 0 ]; then
    cat >>~/.bashrc <<EOF

PS1_COLORIZED="red"
EOF
  fi

  cat >>~/.bashrc <<EOF

#export PS1_TWO_LINES=1
if [ -f ~/.local/lnx-tools/dotfiles/exbashrc ]; then source ~/.local/lnx-tools/dotfiles/exbashrc; fi
if [ -f ~/.local/lnx-tools/dotfiles/myrc ]; then source ~/.local/lnx-tools/dotfiles/myrc; fi
EOF

fi

# update .zshrc if existing
if [ -f ~/.zshrc ]; then
  grep "dotfiles/myrc" ~/.zshrc >/dev/null 2>&1
  if [ $? -eq 1 ]; then
    echo "Adding lnx-tools resources to .zshrc..."
    cat >>.zshrc <<EOF

if [ -f ~/.local/lnx-tools/dotfiles/myrc ]; then source ~/.local/lnx-tools/dotfiles/myrc; fi
EOF

  fi
fi

# Install our own .tmux.conf
if [ ! -f ~/.tmux.conf ]; then
  ln -s ~/.local/lnx-tools/dotfiles/tmux.conf ~/.tmux.conf
fi

# Install our own .vimrc
# Seee pre req with vim.md
if [ ! -f ~/.vimrc ]; then
  ln -s ~/.local/lnx-tools/dotfiles/vimrc ~/.vimrc
fi

if [[ $SHELL =~ "bash" ]]; then source ~/.bashrc; fi
if [[ $SHELL =~ "zsh" ]]; then source ~/.zshrc; fi

echo ""
if [ _${WITH_VIM_AIRLINE}_ == _1_ ]; then
  echo "Installing vim airline..."
  source ~/.local/lnx-tools/dotfiles/myextrafuncs && vim_install_airline
else
  echo -e "To install \e[33mvim_install_airline\e[0m, just execute:"
  echo -e "\e[35msource \$MYLNXTOOLS/dotfiles/myextrafuncs && vim_install_airline\e[0m"
fi

echo ""
if [[ $SHELL =~ "bash" ]]; then echo -e "Don't forget to reload your 'bash' configuration with:\n\e[35msource ~/.bashrc\e[0m"; fi
if [[ $SHELL =~ "zsh" ]]; then echo -e "Don't forget to reload your 'zsh' configuration with:\n\e[35msource ~/.zshrc\e[0m"; fi
echo ""

