# LXC

## Install on Ubuntu

```bash
sudo apt update -y
sudo apt install -y snapd
sudo snap install core lxd

# Enable the current user to use lxd command
sudo adduser $USER lxd
# Set it as effective for the current session
newgrp lxd

```

## Initialize lxd

```bash
lxd init

Would you like to use LXD clustering? (yes/no) [default=no]: 
Do you want to configure a new storage pool? (yes/no) [default=yes]: 
Name of the new storage pool [default=default]: 
Name of the storage backend to use (btrfs, dir, lvm, ceph) [default=btrfs]: 
Create a new BTRFS pool? (yes/no) [default=yes]: 
Would you like to use an existing empty block device (e.g. a disk or partition)? (yes/no) [default=no]: 
Size in GB of the new loop device (1GB minimum) [default=5GB]: 
Would you like to connect to a MAAS server? (yes/no) [default=no]: 
Would you like to create a new local network bridge? (yes/no) [default=yes]: 
What should the new bridge be called? [default=lxdbr0]: 
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: 
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: 
Would you like LXD to be available over the network? (yes/no) [default=no]: yes
Would you like stale cached images to be updated automatically? (yes/no) [default=yes] no
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]: yes
```

or config file

```yaml
config:
  core.https_address: '[::]:8443'
  core.trust_password: lxd
  images.auto_update_interval: "0"
networks:
- config:
    ipv4.address: auto
    ipv6.address: auto
  description: ""
  name: lxdbr0
  type: ""
  project: default
storage_pools:
- config:
    size: 5GB
  description: ""
  name: default
  driver: btrfs
profiles:
- config: {}
  description: ""
  devices:
    eth0:
      name: eth0
      network: lxdbr0
      type: nic
    root:
      path: /
      pool: default
      type: disk
  name: default
cluster: null
```

## Optional proxy
```bash
lxc config set core.proxy_http http://localhost:3128
lxc config set core.proxy_https http://localhost:3128

lxc config show
```

## Profile creation

e.g.

```bash
LXC_PROFILE=kubernetes
# create the new profile
lxc profile create $LXC_PROFILE
# set some parameters
lxc profile set $LXC_PROFILE security.privileged true
lxc profile set $LXC_PROFILE security.nesting true
lxc profile set linux.kernel_modules ip_tables,ip6_tables,netlink_diag,nf_nat,overlay
# show profiles parameters
lxc profile show $LXC_PROFILE
```

## Profile usage

### Add a profile to a newly container to be created

e.g.: create ```microk8s``` container with ```kubernetes``` profile

```bash
lxc launch images:ubuntu/18.04 microk8s -p kubernetes
```

### Add a profile to an existing container

e.g.: add ```kubernetes``` profile to ```microk8s``` container

```bash
lxc profile add microk8s kubernetes
```

Show profiles on a container

```bash
lxc info microk8s | grep Profiles
```
