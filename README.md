# Linux Tools

* [lnx-tools](lnx-tools.md)
* [vim](vim.md)
* [zsh](zsh.md)
* [lxc](lxc.md)

# Initialize/Update ```lnx-tools``` for a new user

```bash
# Basic functions
curl -s https://gitlab.com/api/v4/projects/15486379/repository/files/lnx-tools.sh/raw?ref=master | bash

# Basic functions w/o installing common packages
curl -s https://gitlab.com/api/v4/projects/15486379/repository/files/lnx-tools.sh/raw?ref=master | NO_INSTALL_COMMON_PACKAGES=1 bash

# Basic functions + vim_airline
curl -s https://gitlab.com/api/v4/projects/15486379/repository/files/lnx-tools.sh/raw?ref=master | WITH_VIM_AIRLINE=1 bash

# Basic functions w/o installing common packages + vim_airline
curl -s https://gitlab.com/api/v4/projects/15486379/repository/files/lnx-tools.sh/raw?ref=master | NO_INSTALL_COMMON_PACKAGES=1 WITH_VIM_AIRLINE=1 bash

```


