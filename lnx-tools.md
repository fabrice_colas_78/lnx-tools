# Pre requisites

```bash
sudo apt update -y  && sudo apt install -y git tmux vim curl
```

## Initialize a new environment with bash

```bash
mkdir -p ~/.local && cd ~/.local
# git clone https://gitlab.com/fabrice_colas_78/lnx-tools.git
git clone git@gitlab.com:fabrice_colas_78/lnx-tools.git
cd lnx-tools
source dotfiles/myextrafuncs
git_set_local_user
cd ~/

# Update ~/.ssh/rc file for setting SSH auth socket location for TMUX
grep 'Fix SSH auth socket' ~/.ssh/rc >/dev/null 2>&1
RES_GREP=`echo $?`
if [ $RES_GREP -ne 0 ]; then
  SSH_RC=~/.ssh/rc
  echo Updating $SSH_RC for setting SSH auth socket location ...
  echo >>$SSH_RC
  echo "# Fix SSH auth socket location so agent forwarding works with tmux" >>$SSH_RC
  echo "if test \"\$SSH_AUTH_SOCK\" ; then" >>$SSH_RC
  echo "  ln -sf \$SSH_AUTH_SOCK ~/.ssh/ssh_auth_sock" >>$SSH_RC
  echo "fi" >>$SSH_RC
fi

# update .bashrc
grep "dotfiles/myrc" ~/.bashrc
if [ $? -eq 1 ]; then
  echo "Adding lnx-tools resources to .bashrc..."
  cat >>.bashrc <<EOF

#export PS1_TWO_LINES=1
if [ -f ~/.local/lnx-tools/dotfiles/exbashrc ]; then source ~/.local/lnx-tools/dotfiles/exbashrc; fi
if [ -f ~/.local/lnx-tools/dotfiles/myrc ]; then source ~/.local/lnx-tools/dotfiles/myrc; fi
EOF

  if [[ $SHELL =~ "bash" ]]; then source ~/.bashrc; fi
fi

# update .zshrc if existing
if [ -f ~/.zshrc ]; then
  grep "dotfiles/myrc" ~/.zshrc
  if [ $? -eq 1 ]; then
    echo "Adding lnx-tools resources to .zshrc..."
    cat >>.zshrc <<EOF

if [ -f ~/.local/lnx-tools/dotfiles/myrc ]; then source ~/.local/lnx-tools/dotfiles/myrc; fi
EOF

    if [[ $SHELL =~ "zsh" ]]; then source ~/.zshrc; fi
  fi
fi

# Install our own .tmux.conf
if [ ! -f ~/.tmux.conf ]; then
  ln -s ~/.local/lnx-tools/dotfiles/tmux.conf ~/.tmux.conf
fi

# Install our own .vimrc
# Seee pre req with vim.md
if [ ! -f ~/.vimrc ]; then
  ln -s ~/.local/lnx-tools/dotfiles/vimrc ~/.vimrc
fi

# Install vim airline
source $MYLNXTOOLS/dotfiles/myextrafuncs
vim_install_airline

```

## Update an existing environment

```bash
pushd .
golnxtools
git pull
if [[ $SHELL =~ "bash" ]]; then source ~/.bashrc; fi
if [[ $SHELL =~ "zsh" ]]; then source ~/.zshrc; fi
popd
```
